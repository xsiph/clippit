```text
clippit 0.1.2
Chris Dawkins
--------------------------------------------------------------------
Generate video clips from files contained in input directory

FFMPEG is required to be in PATH to run clippit.

Files are output into the directory of where the programs is executed from.

Filenames should end with timestamps in the format of:
    (HH:MM:SS-HH:MM:SS)

Or multiple timestamps with the format of:
    (HH:MM:SS-HH:MM:SS,HH:MM:SS-HH:MM:SS)

Clips will be renamed to the original filename minus the timestamps.
An additional indexing number is added to the end of the filename.
Examples:
    cool_vid(00:04:20-00:10:10).mp4 -> cool_vid_0.mp4
    cool_vid(00:04:20-00:10:10,00:22:00-00:27:30).mp4 -> [cool_vid_0.mp4, cool_vid_1.mp4]
--------------------------------------------------------------------

USAGE:
    clippit [FLAGS] --input <input>

FLAGS:
    -d, --delete     Delete original files when done
    -h, --help       Prints help information
    -V, --version    Prints version information
    -v, --verbose    Show ffmpeg output

OPTIONS:
    -i, --input <input>    Sets the input directory
--------------------------------------------------------------------
```

## Build Instructions
```bash
cargo build --release && cp ./target/release/clippit ~/.local/bin/
```

## Docker Instructions
Build:
```bash
docker build -t xsiph/clippit .
```
Run:
```bash
docker run -v "$PWD:$PWD" -w "$PWD" xsiph/clippit
```
