
use std::fs::DirEntry;
use std::process::Command;
use crate::file_service::{get_captures, delete_file};

pub fn create_clips_from_video(dir_entry: &DirEntry, verbose: bool, delete: bool) {
    let file_name = String::from(dir_entry.file_name().to_str().unwrap());
    let captures = get_captures(&file_name).unwrap();
    let time_stamps = get_time_stamps(captures.get(1).unwrap().as_str());
    for (i, time_stamp) in time_stamps.iter().enumerate() {
        run_ffmpeg_command(
            &dir_entry,
            time_stamp.as_ref().unwrap(),
            &i,
            verbose)
    }
    if delete {
        delete_file(dir_entry)
    }
}

fn run_ffmpeg_command(dir_entry: &DirEntry, time_stamp: &TimeStamp, index: &usize, verbose: bool) {
    let file_name = String::from(dir_entry.file_name().to_str().unwrap());
    let new_file_path =
        get_new_file_path(&file_name, dir_entry.path().to_str().unwrap(), &index);
    let command = Command::new("ffmpeg")
        .arg("-y")
        .arg("-ss")
        .arg(&time_stamp.start)
        .arg("-i")
        .arg(dir_entry.path())
        .arg("-to")
        .arg(&time_stamp.end)
        .arg("-c")
        .arg("copy")
        .arg(new_file_path)
        .output()
        .expect("Error running ffmpeg command");
    if verbose {
        println!("{}", String::from_utf8_lossy(command.stdout.as_slice()));
        println!("{}", String::from_utf8_lossy(command.stderr.as_slice()));
    }
}

fn get_new_file_path(file_name: &str, path: &str, index: &usize) -> String {
    let mut indexing = String::new();
    indexing.push('_');
    indexing.push_str(&index.to_string());
    let new_file_name =
        file_name.replace(get_captures(&file_name).unwrap().get(0).unwrap().as_str(), &indexing);
    return String::from(path.replace(&file_name, &new_file_name));
}

fn get_time_stamps(s: &str) -> Vec<Result<TimeStamp, &str>> {
    let mut time_stamps: Vec<Result<TimeStamp, &str>> = Vec::new();
    if s.contains(",") {
        s.split(",").for_each(|it| {
            time_stamps.push(TimeStamp::new(it));
        })
    } else {
        time_stamps.push(TimeStamp::new(s));
    }
    return time_stamps;
}

struct TimeStamp {
    start: String,
    end: String
}

impl TimeStamp {
    fn new(time_stamps: &str) -> Result<TimeStamp, &str> {
        let split: Vec<&str> = time_stamps.split("-").collect();
        if split.len() != 2 {
            return Err("Bad timestamp format: should be in format (HH:MM:SS-HH:MM:SS");
        }
        return Ok(TimeStamp {
            start: String::from(split[0]),
            end: String::from(split[1])
        });
    }
}
