use std::fs;
use std::fs::DirEntry;
use std::path::Path;
use regex::{Captures, Regex};

pub fn get_files(path: &str) -> Vec<Result<DirEntry, std::io::Error>>{
    let dir = Path::new(path).read_dir().unwrap_or_else(|err| {
        panic!("{}", err)
    });
    dir.into_iter()
        .filter(|entry| {
            entry.as_ref().unwrap().metadata().unwrap().is_file()
        })
        .filter(|entry| {
            let filename = entry.as_ref().unwrap().file_name();
            let extension = get_extension(&filename.to_str().unwrap());
            return match extension {
                Some(extension) => is_video(extension),
                None => false
            }
        })
        .filter(|entry| {
            get_captures(entry.as_ref().unwrap().file_name().to_str().unwrap()).is_some()
        })
        .collect()
}

pub fn delete_file(dir_entry: &DirEntry) {
    fs::remove_file(dir_entry.path()).unwrap_or_else(|err| {
        eprintln!("Error deleting file: {:?}", dir_entry.path().to_str());
        eprintln!("{:?}", err)
    })
}

pub fn get_captures(file_name: &str) -> Option<Captures> {
    let re = Regex::new("\\(([^\\)]+)\\)").unwrap();
    return re.captures(file_name);
}

fn get_extension(file_name: &str) -> Option<&str> {
    Path::new(file_name)
        .extension()
        .and_then(|s| s.to_str())
}

fn is_video(extension: &str) -> bool {
    let videos = vec!["mp4","mkv"];
    return videos.contains(&&*extension.to_lowercase())
}

