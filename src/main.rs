#[macro_use]
extern crate clap;
use clap::App;
use clippit::{
    validate_system_conditions,
    video_service::create_clips_from_video,
    file_service::get_files,
};

fn main() -> Result<(), &'static str> {
    validate_system_conditions()?;
    let yaml = load_yaml!("cli.yaml");
    let matches = App::from_yaml(yaml).get_matches();
    let input = matches.value_of("input")
        .expect("Input must be specified");
    let delete = matches.is_present("delete");
    let verbose = matches.is_present("verbose");
    get_files(input)
        .iter()
        .filter_map(|it| it.as_ref().ok())
        .for_each(|it|
            create_clips_from_video(&it, verbose, delete)
        );
    Ok(())
}
