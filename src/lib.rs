use std::process::Command;

pub mod file_service;
pub mod video_service;

pub fn validate_system_conditions() -> Result<(), &'static str> {
    let command = Command::new("ffmpeg").arg("-version").output().expect("");
    return match String::from_utf8_lossy(&command.stdout.as_slice())
        .contains("ffmpeg version") {
        true => Ok(()),
        false => Err("ffmpeg must be installed for clippit to function"),
    };
}
